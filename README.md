## Node excercise using GET, POST, PUT and DELETE routes to manage collections in a MongoDB collection.
## Note: database, collections and documents hosted in MongoDB Atlas.
## Version 1
## - Took a 2 year old version of the course and uploaded code up to User Management routes
##
## Version 2
## - Rewritten all code up to Simple Log In feature because teacher updated whole course
##
## Version 3
## - Google Sign In and Sign Out using GCP API credentials
##
## Version 4
## - Added Categories and Products collections plua a basic Search feature
##
## Version 5
## - Added a basic Upload Image for Users and Products