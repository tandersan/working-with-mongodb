// Requires
const path = require('path');
const { v4: uuidv4 } = require('uuid');

// MAIN
const subirArchivo = (files, extensionesValidasArr = ['png', 'txt', 'jpg'], subCarpeta = '') => {
    return new Promise((resolve, reject) => {
        const { archivo } = files;
        const rutaGuardarArchivos = path.join(__dirname, '/../', process.env.FILESPATH, '/', subCarpeta, '/');
        //const rutaFullArchivoAGuardar = rutaGuardarArchivos + archivo.name;
    
        // Yes, I know this only works for NAME.EXT vvv
        const archivoPartidoArr = archivo.name.split('.');
        const archivoExt = archivoPartidoArr[archivoPartidoArr.length-1];
        const nombreTmp = uuidv4() + '.' + archivoExt;
        const rutaFullArchivoAGuardarTmp = rutaGuardarArchivos + nombreTmp;
        // Yes, I know this only works for NAME.EXT ^^^

        if (!extensionesValidasArr.includes(archivoExt)) {
            return reject(`File extension ${archivoExt} is not valid`);
        }

        archivo.mv(rutaFullArchivoAGuardarTmp, function(err) {
            if (err) {
                return reject(err);
            }
    
            resolve({rutaGuardarArchivos, nombreTmp});
        });
    })
}

module.exports = {
    subirArchivo
}