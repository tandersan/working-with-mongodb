// Variables
const User = require('../models/user');
const Role = require('../models/role');
const { modeloUser, modeloRole, modeloCategory, modeloProduct } = require('../models');


// MAIN
const esRolValido = async (role = '') => {
    const existeRol = await modeloRole.findOne({role});
    if (!existeRol) {
        throw new Error('err-007');
    };
};

const existeEmail = async (email = '') => {
    const existeEmailBBDD = await modeloUser.findOne({email});
    if (existeEmailBBDD) {
        throw new Error('err-008');
    };
};

const existeUsuario = async (id = '') => {
    const existeUsuarioBBDD = await modeloUser.findById(id);
    if (!existeUsuarioBBDD) {
        throw new Error('err-012');
    };
};

const existeCategoria = async (id = '') => {
    const existeCategoriaBBDD = await modeloCategory.findById(id);
    if (!existeCategoriaBBDD) {
        throw new Error('err-103');
    };
};

const existeProducto = async (id = '') => {
    const existeProductoBBDD = await modeloProduct.findById(id);
    if (!existeProductoBBDD) {
        throw new Error('err-203');
    };
};

const validarColecciones = (coleccion = '', coleccionesValidas = []) => {
    const esColeccionValida = coleccionesValidas.includes(coleccion);

    if (!esColeccionValida) {
        throw new Error('err-301');
    };
    return true;
}

module.exports = {
    esRolValido,
    existeEmail,
    existeUsuario,
    existeCategoria,
    existeProducto,
    validarColecciones
};