// Requires
const jwt = require('jsonwebtoken');

// MAIN
const generarJWT = (uid = '') => {
    return new Promise((resolve, reject) => {
        const payload = {uid};

        jwt.sign(payload, process.env.SEMILLA_TOKEN, {
            expiresIn: process.env.EXPIRACION_TOKEN
        }, (err, token) => {
            if (err) {
                console.log(err);
                reject( 'No se pudo generar el token' );
            } else {
                resolve(token);
            }
        })
    })
}

module.exports = {
    generarJWT
}