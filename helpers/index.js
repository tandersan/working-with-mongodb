// Requires
const fxs = require('./fx');
const generarJwt = require('./generar-jwt');
const googleVerify = require('./google-verify');
const uploadFiles = require('./upload-files');

//MAIN
module.exports = {
    ...fxs,
    ...generarJwt,
    ...googleVerify,
    ...uploadFiles
}