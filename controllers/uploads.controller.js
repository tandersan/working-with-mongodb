// Requires
const { request, response } = require('express');
const path = require('path');
const fs = require('fs');
const { subirArchivo } = require('../helpers');
const { modeloUser, modeloProduct } = require('../models');

//MAIN
// Just uploading a file ...
const cargarArchivo = async (req = request, res = response) => {
    try {
        //const {rutaGuardarArchivos, nombreTmp} = await subirArchivo(req.files, undefined, 'imagenes');
        const {rutaGuardarArchivos, nombreTmp} = await subirArchivo(req.files);

        res.json({
            msg: `File ${nombreTmp} succesfully uploaded`,
            ok: true,
            filename: nombreTmp,
            directoryWindows: rutaGuardarArchivos.replace(/\\/g, String.fromCharCode(47)),
            directoryLinux: rutaGuardarArchivos
        });
    } catch(msg) {
        return res.status(400).json({
            ok: false,
            err: {
                message: msg
            }
        })
    }
};

// Uploading an Imagen and Updating Users and Products database info
const actualizarImagen = async (req, res = response) => {
    const {coleccion, id} = req.params;
    let modelo;
    let mensagePorColeccion;
    let nombrePorColeccion;

    switch(coleccion) {
        case 'users':
            modelo = await modeloUser.findById(id);

            if (!modelo) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: `The User ID don't exists in the database`
                    }
                })               
            } else {
                mensagePorColeccion = 'user';
                nombrePorColeccion = modelo.nombre + ' ' + modelo.apellido;
            }
        break;

        case 'products':
            modelo = await modeloProduct.findById(id);

            if (!modelo) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: `The Product don't exist in the database`
                    }
                })               
            } else {
                mensagePorColeccion = 'product';
                nombrePorColeccion = modelo.nombre;
            }
        break;

        default:
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'MongoDB colecction not valid'
                }
            })
    }

    // Cleaning previous images
    if (modelo.img) {
        const rutaImagenExistente = path.join(__dirname, '/../', process.env.FILESPATH, '/', coleccion, '/', modelo.img);
        if (fs.existsSync(rutaImagenExistente)) {
            fs.unlinkSync(rutaImagenExistente)
        }
    }

    const {rutaGuardarArchivos, nombreTmp} = await subirArchivo(req.files, undefined, coleccion);
    modelo.img = nombreTmp;
    modelo.save();

    res.json({
        msg: `Image ${nombreTmp} succesfully updated for ${mensagePorColeccion} ${nombrePorColeccion}`,
        ok: true,
        filename: nombreTmp,
        directoryWindows: rutaGuardarArchivos.replace(/\\/g, String.fromCharCode(47)),
        directoryLinux: rutaGuardarArchivos,
        modelo
    });
}

// Showing uploaded image
const mostrarImagen = async (req, res = response) => {
    const {coleccion, id} = req.params;
    let modelo;

    switch(coleccion) {
        case 'users':
            modelo = await modeloUser.findById(id);

            if (!modelo) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: `The User ID don't exists in the database`
                    }
                })               
            }
        break;

        case 'products':
            modelo = await modeloProduct.findById(id);

            if (!modelo) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: `The Product don't exist in the database`
                    }
                })               
            }
        break;

        default:
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'MongoDB colecction not valid'
                }
            })
    }

    // Returning actual image
    if (modelo.img) {
        const rutaImagenExistente = path.join(__dirname, '/../', process.env.FILESPATH, '/', coleccion, '/', modelo.img);
        if (fs.existsSync(rutaImagenExistente)) {
            return res.sendFile(rutaImagenExistente);
        }
    } else {
        const rutaImagenNoExistente = path.join(__dirname, '/../', process.env.FILESPATH, '/', process.env.NO_IMAGE);
        return res.sendFile(rutaImagenNoExistente);
    }
}

module.exports = {
    cargarArchivo,
    actualizarImagen,
    mostrarImagen
};