// Requires
const { request, response } = require('express');
const { modeloCategory } = require('../models');

//MAIN
// Obtener todas las Categorías
const obtenerCategorias = async (req = request, res = response) => {
    let {desde = 0, limite = 3} = req.query;
    (Number.isInteger(parseInt(desde))) ? desde = parseInt(desde) : desde = 0;
    (Number.isInteger(parseInt(limite))) ? limite = parseInt(limite) : limite = 3;
    const condicion = {estado: true};
    
    const [total, categoriasDB] = await Promise.all([
        modeloCategory.countDocuments(condicion),
        modeloCategory.find(condicion)
                      .populate({path: 'usuario', select: 'nombre'})
                      .skip(desde)
                      .limit(limite)
    ]);

    res.json({
        msg:'Full list of Categories',
        ok: true,
        total,
        categorias: categoriasDB
    });
};

// Obtener una única Categoría
const obtenerCategoria = async (req = request, res = response) => {
    const { id } = req.params;
    const existeCategoriaDB = await modeloCategory.findById(id).populate({path: 'usuario', select: 'nombre'});

    res.json({
        msg:'Categorie selected',
        ok: true,
        categoria: existeCategoriaDB
    });
};

// Crear Categoría
const crearCategoria = async (req = request, res = response) => {
    const nombre = req.body.nombre.toUpperCase();

    // Si existe Categoria enviamos mensaje de error
    const existeCategoriaDB = await modeloCategory.findOne({nombre});

    if (existeCategoriaDB) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'The category is already registered in the database'
            }
        })
    } else {
        // Grabamos la categoría
        let category = new modeloCategory({
            nombre: nombre,
            usuario: req.usuario._id
        });

        category.save((err, categoriaDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            };

            res.status(201).json({
                msg:'Category saved',
                ok: true,
                categoria: categoriaDB
            });
        });
    }
};

// Actualizar Categoría
const actualizarCategorias = async (req = request, res = response) => {
    const { id } = req.params;
    const { estado, usuario, ...resto } = req.body;

    resto.nombre = resto.nombre.toUpperCase();
    resto.usuario = req.usuario._id;

    const categoriaDB = await modeloCategory.findByIdAndUpdate(id, resto, {new:true});

    res.json({
        msg:'Category Updated',
        ok: true,
        usuario: categoriaDB
    });
};

// Eliminar Categoría
const eliminarCategorias = async (req = request, res = response) => {
    const { id } = req.params;

    const categoriaDB = await modeloCategory.findByIdAndUpdate(id, {estado:false}, {new:true});
    const usuarioAutenticado = req.usuario._id;

    res.json({
        msg:'Category Deleted',
        ok: true,
        usuario: categoriaDB,
        usuarioAutenticado
    });
};

module.exports = {
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    actualizarCategorias,
    eliminarCategorias
};