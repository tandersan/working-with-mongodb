// Requires
const { request, response } = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const { generarJWT } = require('../helpers/generar-jwt');
const { googleVerify } = require('../helpers/google-verify');

//MAIN
// Validación usuario y posterior log in
const loginUsuario = async (req = request, res = response) => {
    const { email, password } = req.body;

    try {
        const existeUsuario = await User.findOne({email});

        // Verifing that user exists
        if (!existeUsuario) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: '(User) and / or Password mistmatch'
                }
            });
        // Verifing that the existing user still Enabled
        } else if (!existeUsuario.estado) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'User Disabled'
                }
            });
        // Verifing that the password is correct
        } else if (!bcrypt.compareSync(password, existeUsuario.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'User and / or (Password) mistmatch'
                }
            });
        };

        // Generating JWT
        const token = await generarJWT(existeUsuario.id);

        res.json({
            msg:'Log In OK',
            ok: true,
            usuario: existeUsuario,
            token
        });
    } catch (err) {
        console.log(err);
        return res.status(500);
    }
};

// Google Sing In
const googleSignIn = async (req = request, res = response) => {
    const { id_token } = req.body;

    try {
        const { aud, given_name, family_name, email, picture } = await googleVerify(id_token);
        const existeUsuario = await User.findOne({email});

        if (!existeUsuario) {
            let user = new User({
                google: true,
                nombre: given_name,
                apellido: family_name,
                edad: 1,
                email: email,
                role: 'USER_ROLE',
                password: 'no-pass',
                img: picture
            });            

            // Grabamos el Usuario
            await user.save();
        } else if (!existeUsuario.estado) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'User Disabled'
                }
            });
        }

        // Generating JWT
        const token = await generarJWT(existeUsuario.id);

        res.json({
            msg:'Google Id Log In OK',
            ok: true,
            existeUsuario,
            token
        });
    } catch (err) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Google User couldn\'t be verified',
                err
            }
        });
    }
}

module.exports = {
    loginUsuario,
    googleSignIn
};