// Requires
const { request, response } = require('express');
const { ObjectId } = require('mongoose').Types;
const { modeloCategory, modeloProduct, modeloRole, modeloUser } = require('../models');
const coleccionesExistentes = [
    'categories',
    'products',
    'roles',
    'users'
]

const modelosExistentes = [
    'modeloCategory',
    'modeloProduct',
    'modeloRole',
    'modeloUser'
]

//MAIN
const buscar = async (req = request, res = response) => {
    const { coleccion, termino } = req.params;

    // Es la colección una válida? Es decir, existe en MongoDB?
    if (!coleccionesExistentes.includes(coleccion)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: `Allowed collections are ${ coleccionesExistentes }`
            }
        })
    // Si la colección es válida ...
    } else {
        const isValidMongoId = ObjectId.isValid(termino);
        const indice = coleccionesExistentes.findIndex((col) => col === coleccion);

        // Preguntamos si se pregunta por un MongoID válido
        // En caso de SI - buscamos por ID en la colección enviada
        if (isValidMongoId) {
            const resultado = await eval(`${modelosExistentes[indice]}.findById(termino).populate('categoria').populate('usuario')`);

            // Encontramos!
            if (resultado) {
                res.json({
                    msg:`Results for ID [${termino}] in collection [${coleccionesExistentes[indice]}] done!`,
                    ok: true,
                    results: [resultado]
                });
            // No encontramos :(
            } else {
                res.json({
                    msg:`NO results for ID [${termino}] in collection [${coleccionesExistentes[indice]}]`,
                    ok: false,
                    results: []
                });
            }
        // En caso de NO - asumamos que se busca por otro campo en MongoDB
        } else {
            const terminoInsensitive = new RegExp( termino, 'i');
            const resultado = await eval(`${modelosExistentes[indice]}.find({$or:[\
                {nombre: terminoInsensitive},\
                {apellido: terminoInsensitive},\
                {email: terminoInsensitive},\
                {role: terminoInsensitive},\
                {descripcion: terminoInsensitive}\
            ]}).populate('categoria').populate('usuario')`);

            // Encontramos!
            if (resultado) {
                res.json({
                    msg:`Results for [${termino}] in collection [${coleccionesExistentes[indice]}] done!`,
                    ok: true,
                    results: resultado
                });
            // No encontramos :(
            } else {
                res.json({
                    msg:`NO results for [${termino}] in collection [${coleccionesExistentes[indice]}]`,
                    ok: false,
                    results: []
                });
            }
        }
    }
};

module.exports = {
    buscar
};