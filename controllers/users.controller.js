// Requires
const { request, response } = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');

//MAIN
// Crear Usuario
const crearUsuario = async (req = request, res = response) => {
    let body = req.body;

    let user = new User({
        nombre: body.nombre,
        apellido: body.apellido,
        edad: body.edad,
        email: body.email,
        role: body.role,
        password: bcrypt.hashSync(body.password, 10)
    });

    // Grabamos el Usuario
    user.save((err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        };

        res.json({
            msg:'Saving a User in MongoDB using POST',
            ok: true,
            usuario: userDB
        });
    });
};

// Actualizar Usuario
const actualizarUsuarios = async (req = request, res = response) => {
    const { id } = req.params;
    const { _id, password, google, email, ...resto } = req.body;

    if (password) {
        resto.password = bcrypt.hashSync(password, 10)
    };

    const userDB = await User.findByIdAndUpdate(id, resto);

    res.json({
        msg:'Updating a User in MongoDB using PUT',
        ok: true,
        usuario: userDB
    });
};

// Obtener todos los Usuarios
const obtenerUsuarios = async (req = request, res = response) => {
    let {desde = 0, limite = 3} = req.query;
    (Number.isInteger(parseInt(desde))) ? desde = parseInt(desde) : desde = 0;
    (Number.isInteger(parseInt(limite))) ? limite = parseInt(limite) : limite = 3;
    const condicion = {estado: true};
    
    const [total, usuariosDB] = await Promise.all([
        User.countDocuments(condicion),
        User.find(condicion)
            .skip(desde)
            .limit(limite)
    ]);

    res.json({
        msg:'Getting full list of Users from MongDB',
        ok: true,
        total,
        usuarios: usuariosDB
    });
};

// Eliminar Usuario
const eliminarUsuarios = async (req = request, res = response) => {
    const { id } = req.params;

    const usuariosDB = await User.findByIdAndUpdate(id, {estado:false});
    const usuarioAutenticado = req.usuario;

    res.json({
        msg:'Deleting User from MongoDB',
        ok: true,
        usuario: usuariosDB,
        usuarioAutenticado
    });
};

module.exports = {
    obtenerUsuarios,
    crearUsuario,
    actualizarUsuarios,
    eliminarUsuarios
};