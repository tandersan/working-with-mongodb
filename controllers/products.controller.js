// Requires
const { request, response } = require('express');
const { modeloProduct } = require('../models');

//MAIN
// Obtener todos los Productos
const obtenerProductos = async (req = request, res = response) => {
    let {desde = 0, limite = 3} = req.query;
    (Number.isInteger(parseInt(desde))) ? desde = parseInt(desde) : desde = 0;
    (Number.isInteger(parseInt(limite))) ? limite = parseInt(limite) : limite = 3;
    const condicion = {estado: true};
    
    const [total, productosDB] = await Promise.all([
        modeloProduct.countDocuments(condicion),
        modeloProduct.find(condicion)
                     .populate({path: 'usuario', select: 'nombre'})
                     .skip(desde)
                     .limit(limite)
    ]);

    res.json({
        msg:'Full list of Products',
        ok: true,
        total,
        productos: productosDB
    });
};

// Obtener un único Producto
const obtenerProducto = async (req = request, res = response) => {
    const { id } = req.params;
    const existeProductoDB = await modeloProduct.findById(id).populate({path: 'usuario', select: 'nombre'});

    res.json({
        msg:'Product selected',
        ok: true,
        producto: existeProductoDB
    });
};

// Crear Producto
const crearProducto = async (req = request, res = response) => {
    //const nombre = req.body.nombre.toUpperCase();
    const { estado, usuario, ...resto } = req.body;

    // Si existe Categoria enviamos mensaje de error
    const existeProductoDB = await modeloProduct.findOne({nombre: resto.nombre});

    if (existeProductoDB) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'The product is already registered in the database'
            }
        })
    } else {
        // Grabamos el Producto
        let product = new modeloProduct({
            nombre: resto.nombre.toUpperCase(),
            precio: resto.precio,
            usuario: req.usuario._id,
            categoria: resto.idCategoria,
            descripcion: resto.descripcion.trim()
        });

        product.save((err, productDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            };

            res.status(201).json({
                msg:'Product saved',
                ok: true,
                producto: productDB
            });
        });
    }
};

// Actualizar Producto
const actualizarProductos = async (req = request, res = response) => {
    const { id } = req.params;
    const { estado, usuario, ...resto } = req.body;

    resto.nombre = resto.nombre.toUpperCase();
    resto.usuario = req.usuario._id;

    const productoDB = await modeloProduct.findByIdAndUpdate(id, resto, {new:true});

    res.json({
        msg:'Product Updated',
        ok: true,
        producto: productoDB
    });
};

// Eliminar Producto
const eliminarProductos = async (req = request, res = response) => {
    const { id } = req.params;

    const productoDB = await modeloProduct.findByIdAndUpdate(id, {estado:false}, {new:true});
    const usuarioAutenticado = req.usuario._id;

    res.json({
        msg:'Product Deleted',
        ok: true,
        producto: productoDB,
        usuarioAutenticado
    });
};

module.exports = {
    obtenerProductos,
    obtenerProducto,
    crearProducto,
    actualizarProductos,
    eliminarProductos
};