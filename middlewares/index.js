// Requires
const validaCampos = require('./data-validation');
const validaJWT = require('./jwt-validation');
const validaRoles = require('./role-validation');
const validaArchivos = require('./file-validation');

//MAIN
module.exports = {
    ...validaCampos,
    ...validaJWT,
    ...validaRoles,
    ...validaArchivos
}