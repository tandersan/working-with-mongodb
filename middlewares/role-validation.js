// MAIN
const validandoAdminRole = (req, res, next) => {
    if (!req.usuario) {
        return res.status(500).json({
            ok: false,
            err: {
                message: 'Trying to validate Admin Rol without a valid JWT'
            }
        });
    }

    const {role, nombre} = req.usuario;

    if (role !== "ADMIN_ROLE") {
        return res.status(401).json({
            ok: false,
            err: {
                message: `User ${nombre} is not an Admin`
            }
        });
    }

    next();
}

const tieneRolValido = (...roles) => {
    return (req, res, next) => {
        if (!req.usuario) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Trying to validate Admin Rol without a valid JWT'
                }
            });
        }

        if (!roles.includes(req.usuario.role)) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: `User ${req.usuario.nombre} do not have a valid Role`
                }
            });
        }

        next();
    }
}

module.exports = {
    validandoAdminRole,
    tieneRolValido
}