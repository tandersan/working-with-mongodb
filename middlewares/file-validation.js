// Requires
const fs = require('fs');

// Variables
let listaErr = './errList.json';

// MAIN
const validandoArchivo = async (req, res, next) => {
    // Obtenemos la lista de errores definidos para la aplicación
    try {
        jsonErrores = JSON.parse(fs.readFileSync(listaErr, {encoding: 'utf-8'}));
    } catch (err) {
        console.log(err);
    }

    // Si no viene archivo adjunto arroje error
    let errFound = jsonErrores.listErrors.find(codLista => codLista.cod === 'err-401')
    if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
        return res.status(400).json({
            ok: false,
            err: {
                err_code: errFound.cod,
                message: errFound.msg
            }
        })
    }

    next();
}

module.exports = {
    validandoArchivo
}