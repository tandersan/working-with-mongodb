// Requires
const jwt = require('jsonwebtoken');
const User = require('../models/user');

// MAIN
const validandoJWT = async (req, res, next) => {
    const tokenEnviado = req.header('auth');

    if (!tokenEnviado) {
        return res.status(401).json({
            ok: false,
            err: {
                message: 'There is no token in the request'
            }
        });
    }

    try {
        const {uid} = jwt.verify(tokenEnviado, process.env.SEMILLA_TOKEN);

        usuario = await User.find({_id: uid, estado: true});
        if (usuario.length == 0) {
            return res.status(401).json({
                ok: false,
                message: 'User Disabled or do not exists in database'
            });         
        }
        req.usuario = usuario[0];

        next();
    } catch (err) {
        return res.status(401).json({
            ok: false,
            err
        });
    }
}

module.exports = {
    validandoJWT
}