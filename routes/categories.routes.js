// Requires
const { Router } = require('express');
const { check } = require('express-validator');
const { obtenerCategorias, obtenerCategoria, crearCategoria, actualizarCategorias, eliminarCategorias } = require('../controllers/categories.controller');
const { existeCategoria } = require('../helpers/fx');
const { validandoCampos, validandoJWT, validandoAdminRole } = require('../middlewares');

// Variables
const router = Router();

// MAIN
// PUBLIC - Get categories full list    
router.get('/all', obtenerCategorias);

// PUBLIC - Get one category
router.get('/:id', [
    check('id', 'err-011').isMongoId(),
    check('id').custom(existeCategoria),
    validandoCampos
], obtenerCategoria);

// PRIVATE - Token Required - Create category
router.post('/', [
    validandoJWT,
    validandoAdminRole,
    check('nombre', 'err-101').trim().notEmpty(),
    validandoCampos
], crearCategoria);

// PRIVATE - Token Required - Update category
router.put('/:id', [
    validandoJWT,
    validandoAdminRole,
    check('id', 'err-011').isMongoId(),
    check('id').custom(existeCategoria),
    check('nombre', 'err-101').trim().notEmpty(),
    validandoCampos
], actualizarCategorias);

// PRIVATE - Token Required - Delete category
router.delete('/:id', [
    validandoJWT,
    validandoAdminRole,
    check('id', 'err-011').isMongoId(),
    check('id').custom(existeCategoria),
    validandoCampos 
], eliminarCategorias);

router.get('*', (req, res) => {
    res.send('404');
})

module.exports = router;