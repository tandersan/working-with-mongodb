// Requires
const { Router } = require('express');
const { check } = require('express-validator');
const { obtenerProductos, obtenerProducto, crearProducto, actualizarProductos, eliminarProductos } = require('../controllers/products.controller');
const { existeCategoria, existeProducto } = require('../helpers/fx');
const { validandoCampos, validandoJWT, validandoAdminRole } = require('../middlewares');

// Variables
const router = Router();

// MAIN
// PUBLIC - Get produdcts full list    
router.get('/all', obtenerProductos);

// PUBLIC - Get one product
router.get('/:id', [
    check('id', 'err-011').isMongoId(),
    check('id').custom(existeProducto),
    validandoCampos
], obtenerProducto);

// PRIVATE - Token Required - Create product
router.post('/', [
    validandoJWT,
    validandoAdminRole,
    check('nombre', 'err-201').trim().notEmpty(),
    check('idCategoria', 'err-011').isMongoId(),
    check('idCategoria').custom(existeCategoria),
    validandoCampos
], crearProducto);

// PRIVATE - Token Required - Update product
router.put('/:id', [
    validandoJWT,
    validandoAdminRole,
    check('id', 'err-011').isMongoId(),
    check('id').custom(existeProducto),
    check('nombre', 'err-101').trim().notEmpty(),
    check('idCategoria', 'err-011').isMongoId(),
    check('idCategoria').custom(existeCategoria),
    validandoCampos
], actualizarProductos);

// PRIVATE - Token Required - Delete product
router.delete('/:id', [
    validandoJWT,
    validandoAdminRole,
    check('id', 'err-011').isMongoId(),
    check('id').custom(existeProducto),
    validandoCampos 
], eliminarProductos);

router.get('*', (req, res) => {
    res.send('404');
})

module.exports = router;