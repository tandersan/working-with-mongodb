// Requires
const { Router } = require('express');
const { check } = require('express-validator');
const { loginUsuario, googleSignIn } = require('../controllers/auth.controller');
const { validandoCampos } = require('../middlewares/data-validation');

// Variables
const router = Router();

// MAIN
// User Log In and Authentication by JWT
router.post('/login', [
    check('password', 'err-004').trim().notEmpty(),
    check('email', 'err-006').isEmail(),
    validandoCampos
]
, loginUsuario);

router.post('/google', [
    check('id_token', 'err-013').trim().notEmpty(),
    validandoCampos
]
, googleSignIn);

module.exports = router;