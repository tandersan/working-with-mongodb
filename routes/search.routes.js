// Requires
const { Router } = require('express');
const { buscar } = require('../controllers/search.controller');

// Variables
const router = Router();

// MAIN
router.get('/:coleccion/:termino', buscar);

router.get('*', (req, res) => {
    res.send('404');
});

module.exports = router;