// Requires
const { Router } = require('express');
const { check } = require('express-validator');
const { obtenerUsuarios, crearUsuario, actualizarUsuarios, eliminarUsuarios } = require('../controllers/users.controller');
const { esRolValido, existeEmail, existeUsuario } = require('../helpers/fx');
const { validandoCampos, validandoJWT, validandoAdminRole, tieneRolValido } = require('../middlewares');

//Variables
const router = Router();

//MAIN
// Crear Usuario
router.post('/', [
    check('nombre', 'err-001').trim().notEmpty(),
    check('apellido', 'err-002').trim().notEmpty(),
    check('edad', 'err-003').trim().notEmpty(),
    check('password', 'err-004').trim().notEmpty(),
    check('password', 'err-005').isLength({min: 5}),
    check('email', 'err-006').isEmail(),
    check('role').custom(esRolValido),
    check('email').custom(existeEmail),
    validandoCampos
], crearUsuario);

// Actualizar Usuario
router.put('/:id', [
    check('id', 'err-011').trim().isMongoId(),
    check('id').custom(existeUsuario),
    check('role').custom(esRolValido),
    validandoCampos
], actualizarUsuarios);

// Obtener todos los Usuarios
router.get('/', obtenerUsuarios);

// Eliminar Usuario
router.delete('/:id', [
    validandoJWT,
    validandoAdminRole,
    tieneRolValido('ADMIN_ROLE', 'USER_ROLE'),
    check('id', 'err-011').trim().isMongoId(),
    check('id').custom(existeUsuario),
    validandoCampos
], eliminarUsuarios);

/*router.get('*', (req, res) => {
    res.send('404');
});*/

module.exports = router;