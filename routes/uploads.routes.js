// Requires
const { Router } = require('express');
const { check } = require('express-validator');
const { cargarArchivo, actualizarImagen, mostrarImagen } = require('../controllers/uploads.controller');
const { validarColecciones } = require('../helpers');
const { validandoCampos, validandoJWT, validandoAdminRole, validandoArchivo } = require('../middlewares');

//Variables
const router = Router();

//MAIN
router.post('/', [
    validandoArchivo,
    validandoJWT,
    validandoAdminRole
]   
, cargarArchivo);

router.put('/:coleccion/:id', [
    validandoArchivo,
    check('id', 'err-011').isMongoId(),
    check('coleccion').custom( c => validarColecciones( c, ['users','products'] ) ),
    validandoCampos
]
, actualizarImagen)

router.get('/:coleccion/:id', [
    check('id', 'err-011').isMongoId(),
    check('coleccion').custom( c => validarColecciones( c, ['users','products'] ) ),
    validandoCampos
]
, mostrarImagen)

module.exports = router;