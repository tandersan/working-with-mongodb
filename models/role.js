// Requires
const { Schema, model } = require('mongoose');

// MAIN
const rolSchema = new Schema({
    role: {
        type: String,
        required: [true, 'User Role must be provided']
    }
});

module.exports = model('Role', rolSchema);