// Variables
const modeloCategory = require('./category');
const modeloProduct = require('./product');
const modeloRole = require('./role');
const modeloUser = require('./user');

// MAIN
module.exports = {
    modeloCategory,
    modeloProduct,
    modeloRole,
    modeloUser
}