// Requires
const mongoose = require('mongoose');

// SCHEMA
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'Category Name must be provided']
    },
    estado: {
        type: Boolean,
        trim: true,
        default: true,
        required: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

// Exclusión de la contraseña y que no se retorne en el JSON
categorySchema.methods.toJSON = function() {
    const { __v, ...resto } = this.toObject();
    return resto;
}

module.exports = mongoose.model('Categorie', categorySchema);