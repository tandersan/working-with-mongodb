// Requires
require('colors');
const express = require('express');
var cors = require('cors');
const fileUpload = require('express-fileupload');
const { dbConexion } = require('../database/config');

//MAIN
class Server {
    constructor() {
        this.app = express();
        this.puerto = process.env.PORT;
        this.publicPath = __dirname + '/../' + process.env.PUBLICPATH;
        this.tmpPath = __dirname + '/../' + process.env.TMPPATH;
        this.routePaths = {
            users: '/users',
            auth: '/auth',
            categories: '/categories',
            products: '/products',
            search: '/search',
            uploads: '/uploads'
        }

        //Database conexion
        this.conectarBBDD();

        //Middlewares
        this.middlewares();

        // Rutas
        this.rutas();
    }

    middlewares() {
        // CORS
        this.app.use(cors());

        // Reading and Parsing "body" data
        this.app.use(express.json());

        // Public Directory
        this.app.use(express.static(this.publicPath));

        // For managing the file upload process
        this.app.use(fileUpload({
            useTempFiles: true,
            tempFileDir: this.tmpPath
        }));
    }

    rutas() {
        this.app.use(this.routePaths.users, require('../routes/users.routes'));
        this.app.use(this.routePaths.auth, require('../routes/auth.routes'));
        this.app.use(this.routePaths.categories, require('../routes/categories.routes'));
        this.app.use(this.routePaths.products, require('../routes/products.routes'));
        this.app.use(this.routePaths.search, require('../routes/search.routes'));
        this.app.use(this.routePaths.uploads, require('../routes/uploads.routes'));
    }

    subirServidor() {
        this.app.listen(this.puerto, () => {
            console.log(`\nServer On Line\nListening port : ${(this.puerto + '').yellow}`);
        });
    }

    async conectarBBDD() {
        await dbConexion();
    }
}

module.exports = Server;