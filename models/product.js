// Requires
const mongoose = require('mongoose');

// SCHEMA
const Schema = mongoose.Schema;

const productoSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'Product Name must be provided']
    },
    estado: {
        type: Boolean,
        trim: true,
        default: true,
        required: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    precio: {
        type: Number,
        default: 0
    },
    categoria: {
        type: Schema.Types.ObjectId,
        ref: 'Categorie',
        required: true
    },
    descripcion: {
        type: String
    },
    disponible: {
        type: Boolean,
        default: true
    },
    img: {
        type: String,
        required: false
    },
});

// Exclusión de la contraseña y que no se retorne en el JSON
productoSchema.methods.toJSON = function() {
    const { __v, estado, ...resto } = this.toObject();
    return resto;
}

module.exports = mongoose.model('Product', productoSchema);